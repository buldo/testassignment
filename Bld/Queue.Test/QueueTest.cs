﻿namespace Queue.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Тесты многопоточной очереди.
    /// </summary>
    /// <remarks>
    /// Я не исследовал вопрос тестирования многопоточного кода.
    /// Просто сделал, как говорил один спикер на конференции - создаем побольше потоков, запускаем, и надеемся, что не упадет.
    /// </remarks>
    [TestClass]
    public class QueueTest
    {
        /// <summary>
        /// Стандартное число входных коллекций/потоков
        /// </summary>
        private const int DefCollectionsCnt = 50;

        /// <summary>
        /// Стандартное число элементов в коллекции/потоке
        /// </summary>
        private const int DefElementsCnt = 1000;

        /// <summary>
        /// Тестирование много входных потоков, один выходной
        /// </summary>
        /// <remarks>
        /// Работаем с Task. Для большей правильности надо бы переписать на Thread
        /// </remarks>
        [TestMethod]
        [Timeout(60000)]
        public void ManyToOneTest()
        {
            var testData = GenerateInCollection(DefCollectionsCnt, DefElementsCnt);
            var tasks = new List<Task>();
            var queue = new MtQueue<int>();
            foreach (var nums in testData)
            {
                tasks.Add(new Task(
                              () =>
                                  {
                                      foreach (var item in nums)
                                      {
                                          queue.Enqueue(item);
                                      }
                                  }));
            }

            var decLict = new List<int>();
            var itemsCount = DefCollectionsCnt * DefElementsCnt;

            tasks.ForEach(t => t.Start());
            for (int i = 0; i < itemsCount; i++)
            {
                decLict.Add(queue.Dequeue());
            }

            CheckOutputElements(new List<List<int>>() { decLict }, DefCollectionsCnt, DefElementsCnt);
        }

        /// <summary>
        /// Проверяем методы, которые тестируют очереди
        /// </summary>
        [TestMethod]
        public void PositiveSelfTest()
        {
            var generatedData = GenerateInCollection(DefCollectionsCnt, DefElementsCnt);
            CheckOutputElements(generatedData, DefCollectionsCnt, DefElementsCnt);
        }

        /// <summary>
        /// Проверяем методы, которые тестируют очереди
        /// </summary>
        [TestMethod]

        public void NegativeSelfTest()
        {
            var generatedData = GenerateInCollection(DefCollectionsCnt, DefElementsCnt);
            generatedData[0].RemoveAt(6);
            try
            {
                CheckOutputElements(generatedData, DefCollectionsCnt, DefElementsCnt);
            }
            catch (Exception)
            {
                return;
            }

            Assert.Fail();
        }

        /// <summary>
        /// Создание тестовых данных
        /// Несколько списков с целыми неповторяющимися значениями, идущими один за одним
        /// </summary>
        private static List<List<int>> GenerateInCollection(int inCollectionCount, int inCollectionElementsCount)
        {
            var inCollection = new List<List<int>>();

            for (int i = 0; i < inCollectionCount; i++)
            {
                var inEls = new List<int>(inCollectionElementsCount);
                for (int j = 0; j < inCollectionElementsCount; j++)
                {
                    inEls.Add((i * inCollectionElementsCount) + j);
                }

                inCollection.Add(inEls);
            }

            return inCollection;
        }

        /// <summary>
        /// Проверяем, что в коллекции есь все необходимые элементы
        /// </summary>
        private static void CheckOutputElements(List<List<int>> toCheck, uint inCollectionCount, uint inCollectionElementsCount)
        {
            var elements = toCheck.SelectMany(o => o).ToList();
            var elementsCnt = inCollectionCount * inCollectionElementsCount;
            Assert.AreEqual(elementsCnt, (uint)elements.Count);
            for (int i = 0; i < elementsCnt; i++)
            {
                Assert.IsTrue(elements.Contains(i), $"i = {i}");
            }
        }
    }
}
