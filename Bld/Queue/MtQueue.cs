﻿namespace Queue
{
    using System.Collections.Generic;

    /// <summary>
    /// Класс многопоточной очереди
    /// Надо сделать очередь с операциями push(T) и T pop(). 
    /// Операции должны поддерживать обращение с разных потоков. Операция push всегда вставляет и выходит. 
    /// Операция pop ждет пока не появится новый элемент. В качестве контейнера внутри можно использовать только стандартную очередь (Queue) . 
    /// </summary>
    /// <remarks>
    /// Изменил интерфейс класса на стандартный
    /// </remarks>
    public class MtQueue<T>
    {
        /// <summary>
        /// Внутренняя очередь для хранения данных
        /// </summary>
        private readonly Queue<T> _internalQueue = new Queue<T>();

        /// <summary>
        /// Объект для lock
        /// </summary>
        private readonly object _accessLock = new object();

        /// <summary>
        /// Положить элемент в очередь
        /// </summary>
        public void Enqueue(T item)
        {
            lock (_accessLock)
            {
                _internalQueue.Enqueue(item);
            }
        }

        /// <summary>
        /// Достать элемент из очереди
        /// </summary>
        public T Dequeue()
        {
            while (true)
            {
                lock (_accessLock)
                {
                    if (_internalQueue.Count > 0)
                    {
                        return _internalQueue.Dequeue();
                    }
                }
            }
        }
    }
}
