﻿namespace ComboCalc.Test
{
    using System;
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class CalcTest
    {
        [TestMethod]
        public void PositiveTest()
        {
            int searchVal = 5;
            var sourceCollection = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var rightRes = new List<Tuple<int, int>> { new Tuple<int, int>(1, 4), new Tuple<int, int>(2, 3) };
            var calc = new Calc();
            var results = calc.Calculate(sourceCollection, searchVal);
            PositiveCheck(rightRes, results, searchVal);
        }

        [TestMethod]
        public void PositiveTestRevert()
        {
            int searchVal = 5;
            var sourceCollection = new List<int> { 9, 8, 7, 6, 5, 4, 3, 2, 1 };
            var rightRes = new List<Tuple<int, int>> { new Tuple<int, int>(1, 4), new Tuple<int, int>(2, 3) };
            var calc = new Calc();
            var results = calc.Calculate(sourceCollection, searchVal);
            PositiveCheck(rightRes, results, searchVal);
        }

        [TestMethod]
        public void PositiveTetWithBigVals()
        {
            int searchVal = 5;
            var sourceCollection = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, int.MaxValue - 1 };
            var rightRes = new List<Tuple<int, int>> { new Tuple<int, int>(1, 4), new Tuple<int, int>(2, 3) };
            var calc = new Calc();
            var results = calc.Calculate(sourceCollection, searchVal);
            PositiveCheck(rightRes, results, searchVal);
        }
        
        private void PositiveCheck(List<Tuple<int, int>> expected, List<Tuple<int, int>> actual, int expectedVal)
        {
            // Предварительная проверка входных данных, как тестовых так и результата
            Assert.IsNotNull(expected);
            Assert.IsNotNull(actual);
            foreach (var pair in expected)
            {
                Assert.AreEqual(expectedVal, pair.Item1 + pair.Item2);
            }

            foreach (var pair in actual)
            {
                Assert.AreEqual(expectedVal, pair.Item1 + pair.Item2);
            }

            CollectionAssert.AllItemsAreUnique(actual);
            CollectionAssert.AreEquivalent(expected, actual); // Да, может зафейлиться, если порядок чисел в парах не тот...
        }
    }
}
