﻿namespace ComboCalc
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    /// <summary>
    /// Класс для поиска пар
    /// </summary>
    public class Calc
    {
        /// <summary>
        /// Нахождение пар чисел
        /// </summary>
        /// <param name="source">Исходная коллекция</param>
        /// <param name="value">Число для поиска</param>
        /// <returns>Пары из исходной коллекции, дающие в сумме число для поиска</returns>
        public List<Tuple<int, int>> Calculate(List<int> source, int value)
        {
            var retList = new List<Tuple<int, int>>();

            for (int i = 0; i < source.Count; i++)
            {
                var first = source[i];
                for (int j = i; j < source.Count; j++)
                {
                    var second = source[j];
                    try
                    {
                        checked
                        {
                            if (first + second == value)
                            {
                                // Сделаем результат более предсказуемым
                                retList.Add(
                                    first < second
                                        ? new Tuple<int, int>(first, second)
                                        : new Tuple<int, int>(second, first));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Тут может быть только overflow
                        Debug.WriteLine(ex.Message);
                    }
                }
            }

            return retList;
        }
    }
}
